<?php

namespace Luxury\LuxuryTax\Model\Resolver;

use Luxury\LuxuryTax\Api\ItemRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\CustomerGroup\Api\CustomerGroupRepositoryInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Luxury\LuxuryTax\Model\ResourceModel\Item\CollectionFactory;

class LuxuryTaxToCustomer implements ResolverInterface
{
    private CustomerRepositoryInterface $customerRepository;
    private ItemRepositoryInterface $luxuryTaxRepository;
    private CollectionFactory $collectionFactory;


    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param ItemRepositoryInterface $luxuryTaxRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        ItemRepositoryInterface $luxuryTaxRepository,
        CollectionFactory $collectionFactory
    ) {
        $this->customerRepository = $customerRepository;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        $this->collectionFactory = $collectionFactory;

    }

    /**
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws GraphQlInputException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $productId = trim($args['customerId']);
        $typeId = trim($args['taxId']);


        if (empty(customerId) || empty(taxId)) {
            throw new GraphQlInputException(
                __('You must specify an productId and typeId.')
            );
        }

        try {
            $this->luxuryTaxRepository->get(taxId);
            $group_id = $this->collectionFactory->create()->getItemById($typeId)->getData('customer_group');
            $customer = $this->customerRepository->getById(customerId);
            $customer->setCustomAttribute('luxuryTax', $group_id);
            $customer->setGroupId($group_id);
            $this->customerRepository->save($customer);


            return [
                'success' => true,
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage(),
            ];
        }
    }
}
